package app;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    GameBoard root;
    @FXML
    private ScrollPane scroll;
    @FXML
    private VBox board;
    @FXML
    private Button reset;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        root = Main.root;
        reset.setOnAction(evt -> {
            board.getChildren().clear();
            showChildren(root);
        });
    }

    public void showChildren(GameBoard parent) {
        int i = 0;
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.TOP_CENTER);
        hbox.setSpacing(16);
        GameBoard node = parent.childHead;
        do {
            Button button = new Button("x:" + node.x + " y:" + node.y);
            final GameBoard finalNode = node;
            button.setOnAction(evt -> {
                Pane pane = (Pane) button.getParent();

                showChildren(finalNode);
                pane.getChildren().clear();
                pane.getChildren().add(button);
            });
            hbox.getChildren().add(button);
        } while ((node = node.rightSibling) != null);
        board.getChildren().add(hbox);
    }

    ;

}
