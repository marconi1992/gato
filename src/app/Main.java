package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    static boolean hasOptions=true;
    static int deepTree = 0, options = 0;
    static GameBoard root;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Gato");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
        System.out.println(options);
    }


    public static void main(String[] args) {
        root = new GameBoard(GameBoard.ROOT);
        GameBoard node = root;
        while (hasOptions) {
            node = populateNode(node);
        }
        launch(args);
    }

    public static GameBoard populateNode(GameBoard node) {
        GameBoard.refreshBoard();
        do {
            GameBoard child = new GameBoard(GameBoard.CROSSES_PLAYER);
            node.addChild(child);
            child.populateBoard();
            options++;
        } while (node.childTail.searchSite());
        deepTree++;
        if (deepTree < 9) {
            return node.childHead;
        } else {
            return moveLevel(node);
        }

    }

    public static GameBoard moveLevel(GameBoard node) {
        try {
            if (node.rightSibling != null) {
                deepTree--;
                return node.rightSibling;
            } else {
                deepTree--;
                return moveLevel(node.parent);
            }
        }catch (java.lang.NullPointerException ex){
            hasOptions=false;
            return null;
        }

    }
}
