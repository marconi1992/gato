package app;

/**
 * Created by Felipe on 05/11/2015.
 */
public class GameBoard extends Node<GameBoard> {
    public static final int NOUGHTS_PLAYER = 0;
    public static final int CROSSES_PLAYER = 1;
    public static final int ROOT = 2;

    public static final int FREE_SITE = -1;
    public static final int OCCUPIED = -2;


    public static int[][] board = new int[3][3];
    public static int turn = CROSSES_PLAYER;

    public int type;
    public int x;
    public int y;

    public GameBoard(int type) {
        this.type=type;
    }

    public static void refreshBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                board[i][j] = -1;
            }
        }
    }

    public void populateBoard() {
        GameBoard node = this;
        while ((node = node.leftSibling) != null) {
            board[node.y][node.x] = OCCUPIED;
        }
        node = this;
        while ((node = node.parent) != null  && node.type!=ROOT) {
            board[node.y][node.x] = node.type;
        }
    }

    public boolean searchSite() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
               if(board[i][j]==FREE_SITE){
                   x=j;
                   y=i;
                   return  true;
               }
            }
        }
            this.leftSibling.rightSibling = null;
            this.parent.childTail=this.leftSibling;
            this.parent.numChildren--;
            Main.options--;
        return false;
    }

}
