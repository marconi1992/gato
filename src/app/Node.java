package app;

/**
 * Created by Felipe on 05/11/2015.
 */
public class Node<S extends Node> {
    protected   S parent,leftSibling,rightSibling,childHead,childTail;
    protected int numChildren;
    public S getParent() {
        return parent;
    }

    public S getLeftSibling() {
        return leftSibling;
    }

    public S getRightSibling() {
        return rightSibling;
    }

    public int getNumChildren() {
        return numChildren;
    }

    public void addChild(S node){
        node.parent=this;
        if(childHead==null){
            childHead=childTail=node;
        }else{
            node.leftSibling=childTail;
            childTail.rightSibling=node;
            childTail=node;
        }
        numChildren++;
    }

}
